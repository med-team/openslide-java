Source: openslide-java
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Mathieu Malaterre <mathieu.malaterre@gmail.com>
Section: python
Priority: optional
Build-Depends: debhelper (>= 10)
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/med-team/openslide-java
Vcs-Git: https://salsa.debian.org/med-team/openslide-java.git
Homepage: http://openslide.org

Package: libopenslide-java
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${java:Depends}
Recommends: ${java:Recommends}
Description: java wrapper for reading whole slide image files
 OpenSlide is a C library that provides a simple interface to read whole-slide
 images also known as virtual slides.
 .
 Whole-slide images, also known as virtual slides, are large, high resolution
 images used in digital pathology. Reading these images using standard image
 tools or libraries is a challenge because these tools are typically designed
 for images that can comfortably be uncompressed into RAM or a swap file.
 Whole-slide images routinely exceed RAM sizes, often occupying tens of
 gigabytes when uncompressed. Additionally, whole-slide images are typically
 multi-resolution, and only a small amount of image data might be needed at a
 particular resolution.
 .
 This library currently supports:
  * Trestle (.tif)
  * Hamamatsu (.vms, .vmu)
  * Aperio (.svs, .tif)
  * MIRAX (.mrxs)
  * Generic tiled TIFF (.tif)
 .
 This package contains the java module needed to run OpenSlide applications.
